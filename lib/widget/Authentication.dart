import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:personal_expensives_flutterapp/widget/flutter%20fire.dart';
import 'package:personal_expensives_flutterapp/widget/verify_screen.dart';

import 'resource.dart';

class Authentication extends StatefulWidget {
  @override
  _AuthenticationState createState() => _AuthenticationState();
}

class _AuthenticationState extends State<Authentication> {
  TextEditingController emailcontroller = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildPopupDialog(BuildContext context) {
    return new AlertDialog(
      title: const Text('error'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("enter valid email or password"),
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Close'),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          //height: MediaQuery.of(context).size.height,
          width: 200,
          //color: Colors.indigo,
          child: Form(
            // ignore: deprecated_member_use
            autovalidateMode: AutovalidateMode.onUserInteraction,
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                TextFormField(
                    controller: emailcontroller,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'something@gmail.com',
                      labelText: 'email',
                      // errorText: validate ? 'Value Can\'t Be Empty' : null,
                    ),
                    validator: MultiValidator([
                      RequiredValidator(errorText: 'required'),
                      EmailValidator(errorText: 'not a valid email')
                    ])),
                SizedBox(
                  height: 5,
                ),
                TextFormField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'enter password',
                      labelText: 'password',
                    ),
                    validator: MinLengthValidator(6,
                        errorText: 'min 6 to 15 characters')),
                SizedBox(
                  height: 5,
                ),
                RaisedButton(
                    child: Text('login'),
                    onPressed: () async {
                      // if(_formKey.currentState.validate()) {
                      // ignore: non_constant_identifier_names
                      await signIn(
                              emailcontroller.text, passwordController.text)
                          .then((value) {
                        print('login $value');
                        if (value == 'invalid-email' ||
                            value == 'user-not-found') {
                          return showDialog(
                            context: context,
                            builder: (BuildContext context) =>
                                _buildPopupDialog(context),
                          );

                        } else {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Assesh()));
                        }
                      });
                      // }
                    }),
                SizedBox(
                  height: 5,
                ),
                RaisedButton(
                    child: Text('register'),
                    onPressed: () async {
                      // ignore: non_constant_identifier_names
                      await register(
                              emailcontroller.text, passwordController.text)
                          .then   ((value) {
                        print('register $value');
                        if (value == 'invalid-email') {
                          return;
                        } else {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => VerifyScreen()));
                        }
                      });
                    })
              ],
            ),
          ),
        ),
      ),
    );
  }

  funcValidator(String email) {
    if (email.isEmpty) {
      return 'field not empty';
    }
    return "";
  }
}
