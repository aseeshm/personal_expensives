import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:personal_expensives_flutterapp/models/Transaction.dart';
import 'package:intl/intl.dart';

class TransactionList extends StatelessWidget {
  final List<Transaction> transactions;
  final Function deletetx;

  TransactionList(this.transactions,this.deletetx);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 300,
      child: ListView.builder(
        itemBuilder: (context, index) {
          return Card(
            elevation: 5,
            margin: EdgeInsets.symmetric(vertical: 5,horizontal: 5),
            child: ListTile(
              leading: CircleAvatar(
                radius: 30,
                child: Padding(
                    padding: EdgeInsets.all(6),
                    child: FittedBox(
                        child: Text('\$${transactions[index].amount}',))),
              ),
              title: Text(
                transactions[index].title,style: TextStyle(fontWeight: FontWeight.bold),
              ),
              subtitle: Text(
                DateFormat.yMMMd().format(transactions[index].date),
              ),
              trailing: IconButton(icon: Icon(Icons.delete),color: Colors.red,
              onPressed: () => deletetx(transactions[index].id),),
            ),
          );
        },
        itemCount: transactions.length,
      ),
    );
  }
}
