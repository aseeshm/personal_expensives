import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:personal_expensives_flutterapp/widget/chart.dart';
import 'package:personal_expensives_flutterapp/widget/new_Transaction.dart';

import '../models/Transaction.dart';
import 'package:personal_expensives_flutterapp/widget/Transaction_list.dart';

import 'Authentication.dart';

class Assesh extends StatefulWidget {
  @override
  _AsseshState createState() => _AsseshState();
}

class _AsseshState extends State<Assesh> {
  final List<Transaction> userTransaction = [
    Transaction(id: 't1', title: 'shoes', amount: 60.20, date: DateTime.now()),
    Transaction(id: 't2', title: 'snacks', amount: 20.30, date: DateTime.now()),
  ];
  final auth = FirebaseAuth.instance;
  List<Transaction> get recentTransaction {
    return userTransaction.where((tx) {
      return tx.date.isAfter(
        DateTime.now().subtract(Duration(days: 7)),
      );
    }).toList();
  }

  void addNewTransaction(String title, double amount) {
    final newTx = Transaction(
        id: DateTime.now().toString(),
        title: title,
        amount: amount,
        date: DateTime.now());
    setState(() {
      userTransaction.add(newTx);
    });
  }

  void startNewTransaction(BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (_) {
          return NewTransaction(addNewTransaction);
        });
  }

  void deleteTransaction(String id) {
    setState(() {
      userTransaction.removeWhere((tx) {
        return tx.id == id;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final appbar = AppBar(
      title: Text('flutter app'),
      actions: [

        IconButton(
          padding: EdgeInsets.all(0),
          alignment: Alignment.center,
          iconSize: 25,
          icon: Icon(
            Icons.logout,
            color: Colors.white,
          ),
          onPressed: () {
            auth.signOut();
            Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => Authentication()));
          },
        ),
      ],
    );
    return Scaffold(
      appBar: appbar,
      body: SingleChildScrollView(
        child: Column(
          children: [
            //   Container(
            //   width: double.infinity,
            //   child: Card(
            //     child: Text('chart!'),
            //     color: Colors.deepOrange,
            //     elevation: 5,
            //     shadowColor: Colors.blueGrey,
            //   ),
            // ),
            //giving width can be done in both ways
            Container(
                height: (MediaQuery.of(context).size.height -
                        appbar.preferredSize.height -
                        MediaQuery.of(context).padding.top) *
                    0.3,
                child: Chart(recentTransaction)),
            Container(
                height: (MediaQuery.of(context).size.height -
                        appbar.preferredSize.height -
                        MediaQuery.of(context).padding.top) *
                    0.7,
                child: TransactionList(userTransaction, deleteTransaction)),
          ],
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => startNewTransaction(context),
      ),
    );
  }
}
