import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:personal_expensives_flutterapp/models/Transaction.dart';
import 'package:personal_expensives_flutterapp/widget/chart_bar.dart';

class Chart extends StatelessWidget {
  List<Transaction> recentTransactons;

  Chart(this.recentTransactons);

  List<Map<String, Object>> get groupedTransactionValues {
    return List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(Duration(days: index));
      var totalSum = 0.0;
      for (var i = 0; i < recentTransactons.length; i++) {
        if (recentTransactons[i].date.day == weekDay.day &&
            recentTransactons[i].date.month == weekDay.month &&
            recentTransactons[i].date.year == weekDay.year) {
          totalSum += recentTransactons[i].amount;
        }
      }
      return {
        'day': DateFormat.E().format(weekDay).substring(0, 1),
        'amount': totalSum
      };
    }).reversed.toList();
  }

  double get maxspending {
    return groupedTransactionValues.fold(0.0, (sum, item) {
      return sum + item['amount'];
    });
  }

  @override
  Widget build(BuildContext context) {
    print(maxspending);
    return Card(
      elevation: 5,
      margin: EdgeInsets.all(5),
      child: Padding(
        padding: EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: groupedTransactionValues.map((e) {
            return Flexible(
                fit: FlexFit.tight,
                child: ChartBar(e['day'], e['amount'],
                    (e['amount'] as double) / maxspending));
          }).toList(),
        ),
      ),
    );
  }
}
