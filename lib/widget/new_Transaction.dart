import 'package:flutter/material.dart';



class NewTransaction extends StatefulWidget {
  final Function addTx;

  NewTransaction(this.addTx);

  @override
  _NewTransactionState createState() => _NewTransactionState();
}

class _NewTransactionState extends State<NewTransaction> {
  final titleController = TextEditingController();

  final amountController = TextEditingController();

  void submitData() {
    final enteredTitle = titleController.text;
    final enteredAmount = double.parse(amountController.text);

    if (enteredTitle.isEmpty || enteredAmount <= 0) {
      return;
    }

    widget.addTx(enteredTitle, enteredAmount);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: Container(
        margin: EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            TextField(
              decoration: InputDecoration(labelText: 'title'),
              controller: titleController,
              onSubmitted: (val) {
                submitData();
              },
            ),
            TextField(
              decoration: InputDecoration(labelText: 'amount'),
              controller: amountController,
              keyboardType: TextInputType.number ,
              onSubmitted: (val) {
                submitData();
              },
            ),
            FlatButton(
              onPressed: submitData,
              child: Text('add transaction'),
              padding: EdgeInsets.all(5),
            ),
            Row(
              children: [
                RaisedButton(child: Text('date',style: TextStyle(color: Colors.black),),
                    color: Colors.indigo,
                    onPressed: (){})
              ],
            )
          ],
        ),
      ),
    );
  }
}
